FROM docker.uclv.cu/alpine:latest
LABEL maintainer="Sidney Farguharson"
LABEL description="For Testing Purposes Only"
RUN apk add --update nginx && \
    rm -rf /var/cache/apk/* && \
    mkdir -p /tmp/nginx/
COPY conf/nginx.conf /etc/nginx/nginx.conf
ADD conf/modules-enabled/50-mod-http-auth-pam.conf /etc/nginx/modules-enabled/
ADD conf/modules-enabled/50-mod-http-echo.conf /etc/nginx/modules-enabled/
ADD conf/modules-enabled/50-mod-http-image-filter.conf /etc/nginx/modules-enabled/
ADD conf/modules-enabled/50-mod-http-upstream-fair.conf /etc/nginx/modules-enabled/
ADD conf/modules-enabled/50-mod-mail.conf /etc/nginx/modules-enabled/
ADD conf/modules-enabled/50-mod-http-dav-ext.conf /etc/nginx/modules-enabled/
ADD conf/modules-enabled/50-mod-http-geoip.conf /etc/nginx/modules-enabled/
ADD conf/modules-enabled/50-mod-http-subs-filter.conf /etc/nginx/modules-enabled/
ADD conf/modules-enabled/50-mod-http-xslt-filter.conf /etc/nginx/modules-enabled/
ADD conf/modules-enabled/50-mod-stream.conf /etc/nginx/modules-enabled/
ADD conf/html/hello_world.html /usr/share/nginx/

EXPOSE 80/tcp

ENTRYPOINT ["nginx"]
CMD ["-g", "daemon off;"]
